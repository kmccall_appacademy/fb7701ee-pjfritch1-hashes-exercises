# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  word_lengths = {}
  str.split.each {|word| word_lengths[word] = word.length}

  word_lengths
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.key(hash.values.max)
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each_key {|key| older[key] = newer[key]}

  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  letter_freq = Hash.new(0)
  word.each_char {|ch| letter_freq[ch] += 1}

  letter_freq
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  why_use_a_hash = {}
  arr.each {|el| why_use_a_hash[el] = 0}

  why_use_a_hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  parity_counts = {even: 0,odd: 0}
  numbers.each do |num|
    if num.even?
      parity_counts[:even] += 1
    else
      parity_counts[:odd] += 1
    end
  end

  parity_counts
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  m_c_vowel = ["a",0]
  vowel_counts = Hash.new(0)
  string.each_char {|ch| vowel_counts[ch] += 1}

  vowel_counts.each do |key, value|
    if (value > m_c_vowel[1]) || value == m_c_vowel[1] && key < m_c_vowel[0]
      m_c_vowel = [key, value]
    end
  end

  m_c_vowel[0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  student_combos = []
  students.select! {|name, bmonth| bmonth > 6}

  student_arr = students.map {|stud, month| stud}
  student_arr.each_index do |idx1|

    student_arr[(idx1 + 1)..-1].each_index do |idx2|
      student_combos << [student_arr[idx1], student_arr[idx1 + idx2 + 1]]
    end
  end

  student_combos
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  populations = Hash.new(0)
  specimens.each {|spec| populations[spec] += 1}

  populations.length ** 2 * populations.values.min / populations.values.max
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_letters = character_count(normal_sign)
  funny_letters = character_count(vandalized_sign)

  funny_letters.each_key {|ltr| return false if normal_letters[ltr] < funny_letters[ltr]}

  true
end

def character_count(str)
  letters = str.gsub(/\W/,"").downcase

  count = Hash.new(0)
  letters.each_char {|ch| count[ch] += 1}

  count
end
